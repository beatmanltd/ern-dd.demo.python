import requests
from flask import Flask,render_template, request


application = Flask(__name__,static_url_path='',static_folder='web/static',template_folder='web/static')
application.debug = True

@application.route('/success', methods=['GET'])
def success():
    secret_key = "TBD"
    request_id = request.args.get('r')
    token = request.args.get('t')
    #token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsInNvdXJjZSI6IldFQiJ9.eyJleHAiOjE2MjE0NjA2NzgsImlhdCI6MTYyMTQ0MjY3OCwic3ViIjoiMGNhYjIwYTY3OGZiNGI1OTkxMGZjOGM5OGM3OGVmNjIiLCJyb2xlcyI6WyJndWVzdCIsIm1lbWJlciJdLCJzY29wZSI6WyJ3ZWJzaXRlIl0sImFwaS1rZXkiOiJVZGZ5UVhuT2RpN2dyUWlZb3BsRWg2NGZlZ285VHpUNGFnOUxlRGQyIn0.esGjveHDgoNysfxMRdeVNZ_qhLSDXlV4v7tmD-zNPx4"
    url = "https://m4omn46la4.execute-api.eu-west-1.amazonaws.com/staging/requests/" + request_id

    payload={}
    headers = {
        'authorizationToken': 'bearer ' + token,
        'secretKey': '{{secret_key}}'
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    data = response.text
    return render_template('success.html', response_data=data)

@application.route('/')
def root():
    return application.send_static_file('index.html')

if __name__ == "__main__":
     application.run()